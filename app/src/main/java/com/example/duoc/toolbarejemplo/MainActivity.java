package com.example.duoc.toolbarejemplo;

import android.app.ProgressDialog;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private List<Item> dataSet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView)findViewById(R.id.listView);

        dataSet = getValues();

        ItemAdapter adapter = new ItemAdapter(this, dataSet);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view,
                                    int position, long arg) {
                Toast.makeText(getApplicationContext(), dataSet.get(position).getUrl(), Toast.LENGTH_LONG).show();
            }
        });


    }

    private List<Item> getValues(){
        List items = new ArrayList();
        items.add(new Item("http://sintoniageek.com/wp-content/uploads/2011/03/avatar-geek-storm1.jpg", "Following",
                "http://www.imdb.com/title/tt0154506/"));
        items.add(new Item("http://assets6.domestika.org/project-items/000/162/676/162676-4-big.jpg?1316703883", "Memento",
                "http://www.imdb.com/title/tt0209144/"));
        items.add(new Item("http://assets0.domestika.org/project-items/000/162/670/162670-7-big.jpg?1316703818", "Batman Begins",
                "http://www.imdb.com/title/tt0372784/"));
        items.add(new Item("http://assets5.domestika.org/project-items/000/162/675/162675-15-big.jpg?1316703867", "The Prestige",
                "http://www.imdb.com/title/tt0482571/"));
        items.add(new Item("http://milrecursos.com/wp-content/uploads/2011/11/11-Avatares-para-Facebook-muy-originales-y-divertidos-avatares-redes-sociales-blancos.jpg", "The Dark Knight",
                "http://www.imdb.com/title/tt0468569/"));
        items.add(new Item("http://andafter.org/media/images/album/design-grafico/facebook/superman-facebook.jpg", "Inception",
                "http://www.imdb.com/title/tt1375666/"));
        items.add(new Item("https://coolgeeksdotcom.files.wordpress.com/2011/06/design-fetish-no-photo-facebook-31.jpg",
                "The Dark Knight Rises", "http://www.imdb.com/title/tt1345836/"));
        items.add(new Item("http://www.dravenstales.ch/wp-content/uploads/2013/01/fb_avatar04_comic-300x190.jpg",
                "The Dark Knight Rises", "http://www.imdb.com/title/tt1345836/"));
        items.add(new Item("http://4.bp.blogspot.com/_il5FBpex0Ys/THPyYIOmwgI/AAAAAAAAAbA/5C4Kp_RhbSk/s1600/6.jpg",
                "The Dark Knight Rises", "http://www.imdb.com/title/tt1345836/"));
        items.add(new Item("http://www.artifacting.com/blog/wp-content/uploads/2010/11/Alien.png",
                "The Dark Knight Rises", "http://www.imdb.com/title/tt1345836/"));
        return items;
    }

}
