package com.example.duoc.toolbarejemplo;
public class Item {

    private String urlImage;
    private String title;
    private String url;

    public Item(String urlImage, String title, String url) {
        this.urlImage = urlImage;
        this.title = title;
        this.url = url;
    }

    public String getImage() {
        return urlImage;
    }

    public void setImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}